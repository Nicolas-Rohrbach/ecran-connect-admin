/**
 * Build the block
 */
(function(blocks, element, data)
{
    var el = element.createElement;

    blocks.registerBlockType('tvconnecteeamu/display-alert', {
        title: 'Affiche les alertes',
        icon: 'smiley',
        category: 'common',

        edit: function() {
            return "Affiche les alertes dans un tableau";
        },
    });
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.data,
));