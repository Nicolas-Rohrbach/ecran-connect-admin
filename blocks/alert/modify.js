/**
 * Build the block
 */
(function(blocks, element, data)
{
    var el = element.createElement;

    blocks.registerBlockType('tvconnecteeamu/modify-alert', {
        title: 'Modifier une alerte',
        icon: 'smiley',
        category: 'common',

        edit: function() {
            return "Modifie une alerte via un formulaire";
        },
    });
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.data,
));