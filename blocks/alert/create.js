/**
 * Build the block
 */
(function(blocks, element, data)
{
    var el = element.createElement;

    blocks.registerBlockType('tvconnecteeamu/add-alert', {
        title: 'Ajouter une alerte',
        icon: 'smiley',
        category: 'common',

        edit: function() {
            return "Ajoute une alerte via un formulaire";
        },
    });
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.data,
));