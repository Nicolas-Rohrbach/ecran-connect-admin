/**
 * Build the block
 */
(function(blocks, element, data)
{
    var el = element.createElement;

    blocks.registerBlockType('tvconnecteeamu/display-information', {
        title: 'Affiche les informations',
        icon: 'smiley',
        category: 'common',

        edit: function() {
            return "Affiche les informations dans un tableau";
        },
    });
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.data,
));