/**
 * Build the block
 */
(function(blocks, element, data)
{
    var el = element.createElement;

    blocks.registerBlockType('tvconnecteeamu/modify-information', {
        title: 'Modifie une information',
        icon: 'smiley',
        category: 'common',

        edit: function() {
            return "Modifie une information via un formulaire";
        },
    });
}(
    window.wp.blocks,
    window.wp.element,
    window.wp.data,
));