<?php

// Login for viewer
define('DB_USER_VIEWER', 'viewer');
define('DB_PASSWORD_VIEWER', 'viewer');

/**
 * Include all scripts
 * (CSS, JS)
 */
function loadScripts()
{
	//jQuery
	wp_enqueue_script('jquery_cdn', 'https://code.jquery.com/jquery-3.4.1.slim.min.js');

	//Bootstrap
	wp_enqueue_style('bootstrap_css', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css');
	wp_enqueue_script('bootstrap_js', 'https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js', array('jquery_cdn'), '', true);

	//CSS
	wp_enqueue_style('plugin_ecran_css', ECRAN_ADMIN_PLUG_PATH.'public/css/style.css', array(), '1.0');

	//JS
	wp_enqueue_script('sortTable_ecran', ECRAN_ADMIN_PLUG_PATH.'public/js/sortTable.js', array('jquery_cdn'), '', true);
	wp_enqueue_script('checkbox_ecran', ECRAN_ADMIN_PLUG_PATH.'public/js/checkbox.js', array('jquery_cdn'), '', true);
	wp_enqueue_script('search_ecran', ECRAN_ADMIN_PLUG_PATH.'public/js/search.js', array('jquery_cdn'), '', true);
}
add_action('wp_enqueue_scripts', 'loadScripts');

/**
 * Create tables in the database (Alert & Information)
 */
function installDatabaseEcranAdmin()
{
	global $wpdb;
	require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );

	$table_name = 'ecran_information';

	$charset_collate = $wpdb->get_charset_collate();

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id INT(10) NOT NULL AUTO_INCREMENT,
			title VARCHAR (40),
			content VARCHAR(280) NOT NULL,
			creation_date datetime DEFAULT NOW() NOT NULL,
			expiration_date datetime NOT NULL,
			author INT(10) NOT NULL,
			type VARCHAR (10) DEFAULT 'text' NOT NULL,
			PRIMARY KEY (id),
			FOREIGN KEY (author) REFERENCES wp_users(ID) ON DELETE CASCADE
		) $charset_collate;";

	dbDelta($sql);

	// With wordpress id = 1 can't be access if we do : /page/1
	$sql = "ALTER TABLE $table_name AUTO_INCREMENT = 2;";
	dbDelta($sql);

	$table_name = 'ecran_alert';

	$sql = "CREATE TABLE IF NOT EXISTS $table_name (
			id INT(10) NOT NULL AUTO_INCREMENT,
			content VARCHAR(280) NOT NULL,
			creation_date datetime DEFAULT NOW() NOT NULL,
			expiration_date datetime NOT NULL,
			author INT(10) NOT NULL,
			PRIMARY KEY (id),
			FOREIGN KEY (author) REFERENCES wp_users(ID) ON DELETE CASCADE
		) $charset_collate;";

	dbDelta($sql);

	// With wordpress id = 1 can't be access if we do : /page/1
	$sql = "ALTER TABLE $table_name AUTO_INCREMENT = 2;";
	dbDelta($sql);
}
add_action('plugins_loaded', 'installDatabaseEcranAdmin');