<?php


namespace Models;


use PDO;

class User extends Model
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $login;

	/**
	 * @var string
	 */
	private $password;

	/**
	 * @var string
	 */
	private $email;

	/**
	 * Get an user with the id
	 *
	 * @param $id   int
	 *
	 * @return bool|User
	 */
	public function get($id)
	{
		$request = $this->getDatabase()->prepare("SELECT ID, user_login, user_pass, user_email FROM wp_users WHERE ID = :id LIMIT 1");

		$request->bindParam(':id', $id, PDO::PARAM_INT);

		$request->execute();

		if ($request->rowCount() > 0) {
			return $this->setEntity($request->fetch());
		}
		return false;
	}

	/**
	 * Create an entity
	 *
	 * @param $data array
	 *
	 * @return User
	 */
	public function setEntity($data)
	{
		$entity = new User();

		$entity->setId($data['ID']);
		$entity->setLogin($data['user_login']);
		$entity->setPassword($data['user_pass']);
		$entity->setEmail($data['user_email']);

		return $entity;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getLogin()
	{
		return $this->login;
	}

	/**
	 * @param string $login
	 */
	public function setLogin($login)
	{
		$this->login = $login;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @param string $password
	 */
	public function setPassword($password)
	{
		$this->password = $password;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}
}