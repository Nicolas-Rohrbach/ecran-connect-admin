<?php


namespace Models;


use PDO;

class Information extends Model
{

	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $title;

	/**
	 * @var string
	 */
	private $content;

	/**
	 * @var string
	 */
	private $createDate;

	/**
	 * @var string
	 */
	private $expirationDate;

	/**
	 * @var User
	 */
	private $author;

	/**
	 * @var string
	 */
	private $type;

	/**
	 * Insert an information in the database
	 *
	 * @return string
	 */
	public function insert()
	{
		$request = $this->getDatabase()->prepare("INSERT INTO ecran_information (title, content, creation_date, expiration_date, author) VALUES (:title, :content, :creationDate, :expirationDate, :userId) ");

		$request->bindValue(':title', $this->getTitle(), PDO::PARAM_STR);
		$request->bindValue(':content', $this->getContent(), PDO::PARAM_STR);
		$request->bindValue(':creationDate', $this->getCreateDate(), PDO::PARAM_STR);
		$request->bindValue(':expirationDate', $this->getExpirationDate(), PDO::PARAM_STR);
		$request->bindValue(':userId', $this->getAuthor()->getId(), PDO::PARAM_INT);

		$request->execute();

		return $this->getDatabase()->lastInsertId();
	}

	/**
	 * Modify an information
	 *
	 * @return int
	 */
	public function update()
	{
		$request = $this->getDatabase()->prepare("UPDATE ecran_information SET title = :title, content = :content, creation_date  =:creationDate, expiration_date = :expirationDate, author = :author WHERE id = :id");

		$request->bindValue(':title', $this->getTitle(), PDO::PARAM_STR);
		$request->bindValue(':content', $this->getContent(), PDO::PARAM_STR);
		$request->bindValue(':creationDate', $this->getCreateDate(), PDO::PARAM_STR);
		$request->bindValue(':expirationDate', $this->getExpirationDate(), PDO::PARAM_STR);
		$request->bindValue(':author', $this->getAuthor()->getId(), PDO::PARAM_INT);
		$request->bindValue(':id', $this->getId(), PDO::PARAM_INT);

		$request->execute();

		return $request->rowCount();
	}

	public function delete()
	{
		$request = $this->getDatabase()->prepare("DELETE FROM ecran_information WHERE id = :id");

		$request->bindValue(':id', $this->getId(), PDO::PARAM_INT);

		$request->execute();

		if ($request->rowCount() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * @param $id
	 *
	 * @return bool | Information
	 */
	public function get($id)
	{
		$request = $this->getDatabase()->prepare("SELECT id, title, content, creation_date, expiration_date, author, type FROM ecran_information WHERE id = :id LIMIT 1");

		$request->bindParam(':id', $id, PDO::PARAM_INT);

		$request->execute();

		if ($request->rowCount() > 0) {
			return $this->setEntity($request->fetch());
		}
		return false;
	}

	/**
	 * @param int $begin
	 * @param int $numberElement
	 *
	 * @return Information[]|void
	 */
	public function getList($begin = 0, $numberElement = 25)
	{
		$request = $this->getDatabase()->prepare("SELECT id, title, content, creation_date, expiration_date, author, type FROM ecran_information LIMIT :begin, :numberElement");

		$request->bindValue(':begin', (int) $begin, PDO::PARAM_INT);
		$request->bindValue(':numberElement', (int) $numberElement, PDO::PARAM_INT);

		$request->execute();

		if ($request->rowCount() > 0) {
			return $this->setEntityList($request->fetchAll());
		}
		return [];
	}

	public function countAll()
	{
		$request = $this->getDatabase()->prepare("SELECT COUNT(*) FROM ecran_information");

		$request->execute();

		return $request->fetch()[0];
	}

	public function setEntityList($dataList)
	{
		$entityList = [];
		foreach ($dataList as $data) {
			$entityList[] = $this->setEntity($data);
		}
		return $entityList;
	}

	/**
	 * Create an entity
	 *
	 * @param $data
	 *
	 * @return Information
	 */
	public function setEntity($data)
	{
		$entity = new Information();
		$user = new User();

		$entity->setId($data['id']);
		$entity->setTitle($data['title']);
		$entity->setContent($data['content']);
		$entity->setCreateDate($data['creation_date']);
		$entity->setExpirationDate($data['expiration_date']);
		$entity->setAuthor($user->get($data['author']));
		$entity->setType($data['type']);

		return $entity;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getTitle()
	{
		return $this->title;
	}

	/**
	 * @param string $title
	 */
	public function setTitle($title)
	{
		$this->title = $title;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getCreateDate()
	{
		return $this->createDate;
	}

	/**
	 * @param string $createDate
	 */
	public function setCreateDate($createDate)
	{
		$this->createDate = $createDate;
	}

	/**
	 * @return string
	 */
	public function getExpirationDate()
	{
		return $this->expirationDate;
	}

	/**
	 * @param string $expirationDate
	 */
	public function setExpirationDate($expirationDate)
	{
		$this->expirationDate = $expirationDate;
	}

	/**
	 * @return User
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param User $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param string $type
	 */
	public function setType($type)
	{
		$this->type = $type;
	}
}