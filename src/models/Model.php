<?php

namespace Models;

use PDO;

/**
 * Class Model
 *
 * Contain all classic function for models
 *
 * @package Models
 */
class Model
{
	/**
	 * @var PDO
	 */
	private static $database;

	/**
	 * Connect to the database
	 */
	private static function setDatabase()
	{
		try {
			self::$database = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_NAME, DB_USER, DB_PASSWORD);
			self::$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);
		} catch (\PDOException $PDOException) {
			die('Error to the database connection');
		}

	}

	/**
	 * Connect to the database for a viewer account
	 */
	private static function setViewerDatabase()
	{
		try {
			self::$database = new PDO('mysql:host=' . DB_HOST . '; dbname=' . DB_NAME, DB_USER_VIEWER, DB_PASSWORD_VIEWER);
			self::$database->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
		} catch (\PDOException $PDOException) {
			die('Error to the database connection');
		}
	}

	/**
	 * Return the connection
	 *
	 * @return PDO
	 */
	protected function getDatabase()
	{
		if (self:: $database == null) {
			self::setDatabase();
		}
		return self::$database;
	}
}