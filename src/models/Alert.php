<?php


namespace Models;


use PDO;

class Alert extends Model
{
	/**
	 * @var int
	 */
	private $id;

	/**
	 * @var string
	 */
	private $content;

	/**
	 * @var string
	 */
	private $createDate;

	/**
	 * @var string
	 */
	private $expirationDate;

	/**
	 * @var User
	 */
	private $author;

	/**
	 * Insert an alert in the database
	 *
	 * @return string | int
	 */
	public function insert()
	{
		$request = $this->getDatabase()->prepare("INSERT INTO ecran_alert (content, creation_date, expiration_date, author) VALUES (:content, :creationDate, :expirationDate, :author)");

		$request->bindValue(':content', $this->getContent(), PDO::PARAM_STR);
		$request->bindValue(':creationDate', $this->getCreateDate(), PDO::PARAM_STR);
		$request->bindValue(':expirationDate', $this->getExpirationDate(), PDO::PARAM_STR);
		$request->bindValue(':author', $this->getAuthor()->getId(), PDO::PARAM_INT);

		$request->execute();

		return $this->getDatabase()->lastInsertId();
	}


	/**
	 * @return int
	 */
	public function update()
	{
		$request = $this->getDatabase()->prepare("UPDATE ecran_alert SET content = :content, creation_date  =:creationDate, expiration_date = :expirationDate, author = :author WHERE id = :id");

		$request->bindValue(':content', $this->getContent(), PDO::PARAM_STR);
		$request->bindValue(':creationDate', $this->getCreateDate(), PDO::PARAM_STR);
		$request->bindValue(':expirationDate', $this->getExpirationDate(), PDO::PARAM_STR);
		$request->bindValue(':author', $this->getAuthor()->getId(), PDO::PARAM_INT);
		$request->bindValue(':id', $this->getId(), PDO::PARAM_INT);

		$request->execute();

		return $request->rowCount();
	}

	/**
	 * @return bool
	 */
	public function delete()
	{
		$request = $this->getDatabase()->prepare("DELETE FROM ecran_alert WHERE id = :id");

		$request->bindValue(':id', $this->getId(), PDO::PARAM_INT);

		$request->execute();

		if ($request->rowCount() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * @param $id
	 *
	 * @return bool|Alert
	 */
	public function get($id)
	{
		$request = $this->getDatabase()->prepare("SELECT id, content, creation_date, expiration_date, author FROM ecran_alert WHERE id = :id");

		$request->bindParam(':id', $id, PDO::PARAM_INT);

		$request->execute();

		if($request->rowCount() > 0) {
			return $this->setEntity($request->fetch());
		}
		return false;
	}

	/**
	 * @param int $begin
	 * @param int $numberElement
	 *
	 * @return array|Alert[]
	 */
	public function getList($begin = 0, $numberElement = 25)
	{
		$request = $this->getDatabase()->prepare("SELECT id, content, creation_date, expiration_date, author FROM ecran_alert ORDER BY id ASC LIMIT :begin, :numberElement");

		$request->bindValue(':begin', (int) $begin, PDO::PARAM_INT);
		$request->bindValue(':numberElement', (int) $numberElement, PDO::PARAM_INT);

		$request->execute();

		if ($request->rowCount() > 0) {
			return $this->setEntityList($request->fetchAll());
		}
		return [];
	}

	/**
	 * @return int
	 */
	public function countAll()
	{
		$request = $this->getDatabase()->prepare("SELECT COUNT(*) FROM ecran_alert");

		$request->execute();

		return $request->fetch()[0];
	}

	/**
	 * @param $dataList
	 *
	 * @return Alert[]
	 */
	public function setEntityList($dataList)
	{
		$entityList = [];
		foreach ($dataList as $data) {
			$entityList[] = $this->setEntity($data);
		}
		return $entityList;
	}

	/**
	 * @param $data
	 *
	 * @return Alert
	 */
	public function setEntity($data)
	{
		$entity = new Alert();
		$user = new User();

		$entity->setId($data['id']);
		$entity->setContent($data['content']);
		$entity->setCreateDate($data['creation_date']);
		$entity->setExpirationDate($data['expiration_date']);
		$entity->setAuthor($user->get($data['author']));

		return $entity;
	}

	/**
	 * @return int
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
		$this->id = $id;
	}

	/**
	 * @return string
	 */
	public function getContent()
	{
		return $this->content;
	}

	/**
	 * @param string $content
	 */
	public function setContent($content)
	{
		$this->content = $content;
	}

	/**
	 * @return string
	 */
	public function getCreateDate()
	{
		return $this->createDate;
	}

	/**
	 * @param string $createDate
	 */
	public function setCreateDate($createDate)
	{
		$this->createDate = $createDate;
	}

	/**
	 * @return string
	 */
	public function getExpirationDate()
	{
		return $this->expirationDate;
	}

	/**
	 * @param string $expirationDate
	 */
	public function setExpirationDate($expirationDate)
	{
		$this->expirationDate = $expirationDate;
	}

	/**
	 * @return User
	 */
	public function getAuthor()
	{
		return $this->author;
	}

	/**
	 * @param User $author
	 */
	public function setAuthor($author)
	{
		$this->author = $author;
	}


}