<?php


namespace Controllers;


use Models\Alert;
use Models\User;
use Views\AlertView;

class AlertController extends Controller
{
	/**
	 * @var Alert
	 */
	private $model;

	/**
	 * @var AlertView
	 */
	private $view;

	/**
	 * InformationController constructor.
	 */
	public function __construct()
	{
		/*
		if(!is_user_logged_in()) {
			wp_redirect(wp_login_url());
			exit;
		}*/
		$this->model = new Alert();
		$this->view = new AlertView();
	}

	/**
	 * @return string
	 */
	public function create() {
		$submit = filter_input(INPUT_POST, 'submit');
		if(isset($submit)) {
			$content        = filter_input(INPUT_POST, 'content');
			$expirationDate = filter_input(INPUT_POST, 'expirationDate');
			if ($this->isRealDate($expirationDate) && date('Y-m-d') < $expirationDate && is_string($content) && strlen($content) >= 4) {

				$this->model->setContent($content);
				$this->model->setCreateDate(date('Y-m-d'));
				$this->model->setExpirationDate($expirationDate);

				$author       = new User();
				$current_user = wp_get_current_user();
				$author->setId($current_user->ID);

				$this->model->setAuthor($author);

				if($this->model->insert()) {
					echo $this->view->buildModal('Création réussie !', 'La création de l\'alerte a été effectuée !', esc_url(get_permalink(get_page_by_title('Gestion des alertes'))));
				} else {
					// @TODO write error
				}
			} else {
				// @TODO write error
			}
		}
		return $this->view->creationForm();
	}

	public function modify()
	{
		$id = $this->getPartOfUrl()[2];
		if(!is_numeric($id) || !$this->model->get($id)) {
			return $this->view->noAlert();
		}

		$entity = $this->model->get($id);

		$submit = filter_input(INPUT_POST, 'submit');
		if(isset($submit)) {
			$content = filter_input(INPUT_POST, 'content');
			$expirationDate = filter_input(INPUT_POST, 'expirationDate');

			if(is_string($content) && strlen($content) >= 4 && $this->isRealDate($expirationDate) && $expirationDate > date('Y-m-d')) {
				$entity->setContent($content);
				$entity->setExpirationDate($expirationDate);

				if($entity->update()) {
					echo $this->view->buildModal('Modification réussie !', 'La modification de l\'alerte a été effectuée !', esc_url(get_permalink(get_page_by_title('Gestion des alertes'))));
				} else {
					// @TODO write error
				}
			} else {
				// @TODO write error
			}
		}

		$delete = filter_input(INPUT_POST, 'delete');
		if(isset($delete)) {
			$entity->delete();
		}
		return $this->view->modifyForm($entity->getContent(), $entity->getExpirationDate());
	}

	public function displayAll()
	{
		$numberAllEntity = $this->model->countAll();
		$url = $this->getPartOfUrl();
		$number = filter_input(INPUT_GET, 'number');
		$pageNumber = 1;
		if(sizeof($url) >= 2 && is_numeric($url[1])) {
			$pageNumber = $url[1];
		}
		if(isset($number) && !is_numeric($number) || empty($number)) {
			$number = 25;
		}
		$begin = ($pageNumber - 1) * $number;
		$maxPage = ceil($numberAllEntity / $number);
		if($maxPage <= $pageNumber) {
			$pageNumber = $maxPage;
		}
		$alertList = $this->model->getList($begin, $number);
		$name = 'Alert';
		$header = ['Contenu', 'Date de création', 'Date d\'expiration', 'Auteur', 'Modifier'];
		$dataList = [];
		$row = $begin;
		foreach ($alertList as $alert) {
			++$row;
			$dataList[] = [$row, $this->view->buildCheckbox($name, $alert->getId()), $alert->getContent(), $alert->getCreateDate(), $alert->getExpirationDate(), $alert->getAuthor()->getLogin(), $this->view->buildLinkForModify(esc_url(get_permalink(get_page_by_title('Modifier une alerte'))).$alert->getId())];
		}

		$submit = filter_input(INPUT_POST, 'delete');
		if(isset($submit)) {
			if (isset($_REQUEST['checkboxStatusAlert'])) {
				$checked_values = $_REQUEST['checkboxStatusAlert'];
				foreach ($checked_values as $id) {
					$entity = $this->model->get($id);
					$entity->delete();
				}
			}
		}
		$returnString = "";
		if($pageNumber === 1) {
			$returnString = $this->view->contextDisplayAll();
		}
		return $returnString.$this->view->displayAll($name, 'Alertes', $header, $dataList).$this->view->pageNumber($maxPage, $pageNumber, esc_url(get_permalink(get_page_by_title('Gestion des alertes'))), $number);
	}
}