<?php

namespace Controllers;

/**
 * Class Controller
 *
 * Contain all classic functions for controllers
 *
 * @package Controllers
 */
class Controller
{
	/**
	 * Explode the url by /
	 *
	 * @return array
	 */
	public function getPartOfUrl()
	{
		$url = $_SERVER['REQUEST_URI'];
		$urlExplode = explode('/', $url);
		$cleanUrl = array();
		for($i = 0; $i < sizeof($urlExplode); ++$i) {
			if($urlExplode[$i] != '/' && $urlExplode[$i] != '') {
				$cleanUrl[] = $urlExplode[$i];
			}
		}
		return $cleanUrl;
	}

	/**
	 * Check if the input is a date
	 *
	 * @param $date
	 *
	 * @return bool
	 */
	public function isRealDate($date)
	{
		if (false === strtotime($date)) {
			return false;
		}
		list($year, $month, $day) = explode('-', $date);
		return checkdate($month, $day, $year);
	}
}