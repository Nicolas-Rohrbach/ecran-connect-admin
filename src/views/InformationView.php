<?php


namespace Views;

/**
 * Class InformationView
 *
 * Contain all view for the information
 *
 * @package Views
 */
class InformationView extends View
{
	/**
	 * Build a form to create an information
	 *
	 * @return string
	 */
	public function creationForm()
	{
		$dateMin = date('Y-m-d', strtotime("+1 day"));
		return '
		<form method="post">
			<div class="form-group">
				<label for="title">Titre <span class="text-muted">(Optionnel)</span></label>
				<input type="text" class="form-control" id="title" name="title" maxlength="40" placeholder="Titre...">
			</div>
			<div class="form-group">
				<label for="content">Contenu</label>
				<input type="text" class="form-control" id="content" name="content" placeholder="280 caractères au maximum" maxlength="280" minlength="4" required>
			</div>
			<div class="form-group">
				<label>Date d\'expiration</label>
				<input type="date" class="form-control" id="expirationDate" name="expirationDate" min="'.$dateMin.'" required>
			</div>
			<button type="submit" class="btn button_ecran" name="submit">Valider</button>
		</form>'.$this->contextCreateInformation();
	}

	/**
	 * Explain how the information's display
	 *
	 * @return string
	 */
	public function contextCreateInformation()
	{
		return '
		<hr class="half-rule">
		<div>
			<h2>Les informations</h2>
			<p class="lead">Lors de la création de votre information, celle-ci sera posté le lendemain sur tous les téléviseurs qui utilisent le projet de l\'écran connecté.</p>
			<p class="lead">Les informations que vous créez seront affichées avec les informations déjà présentes.</p>
			<p class="lead">Les informations sont affichées dans un diaporama défilant les informations une par une sur la partie droite des téléviseurs.</p>
			<div class="text-center">
				<figure class="figure">
					<img src="'.ECRAN_ADMIN_PLUG_PATH.'public/media/presentation.png" class="figure-img img-fluid rounded" alt="Représentation d\'un téléviseur">
					<figcaption class="figure-caption">Représentation d\'un téléviseur</figcaption>
				</figure>
			</div>
		</div>';
	}

	/**
	 * Create a form to modify an information
	 *
	 * @param $title
	 * @param $content
	 * @param $expirationDate
	 *
	 * @return string
	 */
	public function modifyForm($title, $content, $expirationDate)
	{
		$expirationDate = date('Y-m-d', strtotime($expirationDate));
		$dateMin = date('Y-m-d', strtotime("+1 day"));
		return '
		<a href="'.esc_url(get_permalink(get_page_by_title('Gestion des informations'))).'">< Retour</a>
		<form method="post">
			<div class="form-group">
				<label for="title">Titre <span class="text-muted">(Optionnel)</span></label>
				<input type="text" class="form-control" id="title" name="title" maxlength="40" value="'.$title.'">
			</div>
			<div class="form-group">
				<label for="content">Contenu</label>
				<input type="text" class="form-control" id="content" name="content" placeholder="280 caractères au maximum" maxlength="280" minlength="4" value="'.$content.'" required>
			</div>
			<div class="form-group">
				<label>Date d\'expiration</label>
				<input type="date" class="form-control" id="expirationDate" name="expirationDate" min="'.$dateMin.'" value="'.$expirationDate.'" required>
			</div>
			<button type="submit" class="btn button_ecran" name="submit">Valider</button>
			<button type="submit" class="btn button_ecran" name="delete" onclick="return confirm(\' Voulez-vous supprimer le(s) élément(s) sélectionné(s) ?\');">Supprimer</button>
		</form>';
	}

	public function contextDisplayAll()
	{
		return '
		<div class="row">
			<div class="col-6 mx-auto col-md-6 order-md-2">
				<img src="'.ECRAN_ADMIN_PLUG_PATH.'public/media/info.png" alt="Logo information" class="img-fluid mb-3 mb-md-0">
			</div>
			<div class="col-md-6 order-md-1 text-center text-md-left pr-md-5">
				<p class="lead">Vous pouvez retrouver ici toutes les informations qui ont été créées sur ce site.</p>
				<p class="lead">Les informations sont triées de la plus vieille à la plus récente.</p>
				<p class="lead">Vous pouvez modifier une information en cliquant sur "Modifier" à la ligne correspondante à l\'information.</p>
				<p class="lead">Vous souhaitez supprimer une / plusieurs information(s) ? Cochez les cases des informations puis cliquez sur "Supprimer" le bouton ce situe en bas du tableau.</p>
			</div>
		</div>
		<hr class="half-rule">';
	}

	public function noInformation()
	{
		return '
		<a href="'.esc_url(get_permalink(get_page_by_title('Gestion des informations'))).'">< Retour</a>
		<div>
			<h3>Information non trouvée</h3>
			<p>Cette information n\'éxiste pas, veuillez bien vérifier d\'avoir bien cliqué sur une information.</p>
			<p></p>
		</div>';
	}
}