<?php


namespace Views;

/**
 * Class AlertView
 * @package Views
 */
class AlertView extends View
{

	/**
	 * Build a form to create an alert
	 *
	 * @return string
	 */
	public function creationForm()
	{
		$dateMin = date('Y-m-d', strtotime("+1 day"));
		return '
		<form method="post">
			<div class="form-group">
				<label for="content">Contenu</label>
				<input type="text" class="form-control" id="content" name="content" placeholder="280 caractères au maximum" minlength="4" maxlength="280" required>
			</div>
			<div class="form-group">
				<label>Date d\'expiration</label>
				<input type="date" class="form-control" id="expirationDate" name="expirationDate" min="'.$dateMin.'" required>
			</div>
			<button type="submit" class="btn button_ecran" name="submit">Valider</button>
		</form>'.$this->contextCreateAlert();
	}

	/**
	 * Explain how the alert's display
	 *
	 * @return string
	 */
	public function contextCreateAlert()
	{
		return '
		<hr class="half-rule">
		<div>
			<h2>Les alertes</h2>
			<p class="lead">Lors de la création de votre alerte, celle-ci sera posté le lendemain sur tous les téléviseurs qui utilisent le projet de l\'écran connecté.</p>
			<p class="lead">Les alertes que vous créez seront affichées avec les alertes déjà présentes.</p>
			<p class="lead">Les alertes sont affichées les un après les autres défilant à la chaîne en bas des téléviseurs.</p>
			<div class="text-center">
				<figure class="figure">
					<img src="'.ECRAN_ADMIN_PLUG_PATH.'public/media/presentation.png" class="figure-img img-fluid rounded" alt="Représentation d\'un téléviseur">
					<figcaption class="figure-caption">Représentation d\'un téléviseur</figcaption>
				</figure>
			</div>
		</div>';
	}

	/**
	 * Create a form to modify an alert
	 *
	 * @param $content
	 * @param $expirationDate
	 *
	 * @return string
	 */
	public function modifyForm($content, $expirationDate)
	{
		$expirationDate = date('Y-m-d', strtotime($expirationDate));
		$dateMin = date('Y-m-d', strtotime("+1 day"));
		return '
		<a href="'.esc_url(get_permalink(get_page_by_title('Gestion des alertes'))).'">< Retour</a>
		<form method="post">
			<div class="form-group">
				<label for="content">Contenu</label>
				<input type="text" class="form-control" id="content" name="content" placeholder="280 caractères au maximum" minlength="4" maxlength="280" value="'.$content.'" required>
			</div>
			<div class="form-group">
				<label>Date d\'expiration</label>
				<input type="date" class="form-control" id="expirationDate" name="expirationDate" min="'.$dateMin.'" value="'.$expirationDate.'" required>
			</div>
			<button type="submit" class="btn button_ecran" name="submit">Valider</button>
			<button type="submit" class="btn delete_button_ecran" name="delete" onclick="return confirm(\' Voulez-vous supprimer le(s) élément(s) sélectionné(s) ?\');">Supprimer</button>
		</form>'.$this->contextModify();
	}

	public function contextModify()
	{
		return '
		<hr class="half-rule">
		<div>
			<p class="lead">La modification d\'une alerte prend effet comme pour la création, le lendemain.</p>
			<p class="lead">Vous pouvez donc prolonger le temps d\'expiration ou bien modifier le contenu de votre alerte.</p>
		</div>';
	}

	public function contextDisplayAll()
	{
		return '
		<div class="row">
			<div class="col-6 mx-auto col-md-6 order-md-2">
				<img src="'.ECRAN_ADMIN_PLUG_PATH.'public/media/alert.png" alt="Logo alerte" class="img-fluid mb-3 mb-md-0">
			</div>
			<div class="col-md-6 order-md-1 text-center text-md-left pr-md-5">
				<p class="lead">Vous pouvez retrouver ici toutes les alertes qui ont été créées sur ce site.</p>
				<p class="lead mb-4">Les alertes sont triées de la plus vieille à la plus récente.</p>
				<p class="lead mb-4">Vous pouvez modifier une alerte en cliquant sur "Modifier" à la ligne correspondante à l\'alerte.</p>
				<p class="lead mb-4">Vous souhaitez supprimer une / plusieurs alerte(s) ? Cochez les cases des alertes puis cliquez sur "Supprimer" le bouton ce situe en bas du tableau.</p>
			</div>
		</div>
		<hr class="half-rule">';
	}

	public function noAlert()
	{
		return '
		<a href="'.esc_url(get_permalink(get_page_by_title('Gestion des alertes'))).'">< Retour</a>
		<div>
			<h3>Alerte non trouvée</h3>
			<p>Cette alerte n\'éxiste pas, veuillez bien vérifier d\'avoir bien cliqué sur une alerte.</p>
			<p></p>
		</div>';
	}
}