<?php

namespace Views;

/**
 * Class View
 *
 * Contain all classic function for views
 *
 * @package Views
 */
class View
{
	/**
	 * Display a table, showing all element from a database
	 *
	 * @param $name
	 * @param $title
	 * @param $dataHeader
	 * @param $dataList
	 * @param string $idTable
	 *
	 * @return string
	 */
	public function displayAll($name, $title, $dataHeader, $dataList, $idTable = '')
	{
		$name = '\''.$name.'\'';
		$table = '
		<h2>' . $title . '</h2>
		<input type="text" id="key'.$idTable.'" name="key" onkeyup="search(\''.$idTable.'\')" placeholder="Recherche...">
		<form method="post">
			<div class="table-responsive">
				<table class="table table-striped table-hover" id="table'.$idTable.'">
					<thead>
						<tr class="text-center">
							<th width="5%" class="text-center" onclick="sortTable(0, \''.$idTable.'\')">#</th>
		                    <th scope="col" width="5%" class="text-center"><input type="checkbox" onClick="toggle(this, ' . $name . ')" /></th>';

		$count = 1;
		foreach ($dataHeader as $data) {
			++$count;
			$table .= '<th scope="col" class="text-center" onclick="sortTable('.$count.', \''.$idTable.'\')">'.$data.'</th>';
		}

		$table .= '
			</tr>
		</thead>
		<tbody>';

		foreach ($dataList as $data) {
			$table .= '<tr>';
			foreach ($data as $column) {
				$table .= '<td class="text-center">'.$column.'</td>';
			}
			$table .= '</tr>';
		}

		$table .= '
					</tbody>
				</table>
			</div>
	        <input type="submit" class="btn delete_button_ecran" value="Supprimer" name="delete" onclick="return confirm(\' Voulez-vous supprimer le(s) élément(s) sélectionné(s) ?\');"/>
	    </form>';

		return $table;
	}

	public function pageNumber($pageNumber, $currentPage, $url, $numberElement = null)
	{
		$pagination = '
        <nav aria-label="Page navigation example">
            <ul class="pagination">';

		if($currentPage > 1) {
			$pagination .= '
            <li class="page-item">
              <a class="page-link" href="'.$url.'/'.($currentPage - 1).'/?number='.$numberElement.'" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
              </a>
            </li>
            <li class="page-item"><a class="page-link" href="'.$url.'/1/?number='.$numberElement.'">1</a></li>';
		}
		if($currentPage > 3) {
			$pagination .= '<li class="page-item page-link disabled">...</li>';
		}
		for ($i = $currentPage - 3; $i < $currentPage; ++$i) {
			if($i > 1) {
				$pagination .= '<li class="page-item"><a class="page-link" href="'.$url.$i.'/?number='.$numberElement.'">'.$i.'</a></li>';
			}
		}
		$pagination .= '
        <li class="page-item active_ecran" aria-current="page">
          <a class="page-link" href="'.$url.$currentPage.'/?number='.$numberElement.'">'.$currentPage.'<span class="sr-only">(current)</span></a>
        </li>';
		for ($i = $currentPage + 1; $i < $currentPage + 3; ++$i) {
			if($i < $pageNumber) {
				$pagination .= '<li class="page-item"><a class="page-link" href="'.$url.'/'.$i.'/?number='.$numberElement.'">'.$i.'</a></li>';
			}
		}
		if($currentPage < $pageNumber) {
			if($pageNumber - $currentPage > 3) {
				$pagination .= '<li class="page-item page-link disabled">...</li>';
			}
			$pagination .= '
            <li class="page-item"><a class="page-link" href="'.$url.'/'.$pageNumber.'/?number='.$numberElement.'">'.$pageNumber.'</a></li>
            <li class="page-item">
              <a class="page-link" href="'.$url.'/'.($currentPage + 1).'/?number='.$numberElement.'" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
              </a>
            </li>';
		}
		$pagination .= '
          </ul>
        </nav>';
		return $pagination;
	}

	public function buildModal($title, $content, $redirect = null)
	{
		return '
		<!-- MODAL -->
		<div class="modal" id="myModal" tabindex="-1" role="dialog">
		  <div class="modal-dialog modal-dialog-centered" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">'.$title.'</h5>
		      </div>
		      <div class="modal-body">
		        '.$content.'
		      </div>
		      <div class="modal-footer">
		        <a href="'.$redirect.'" class="btn btn-primary">Fermer</a>
		      </div>
		    </div>
		  </div>
		</div>
		
		<script>
			$(\'#myModal\').show();
		</script>';
	}

	/**
	 * Create a checkbox
	 *
	 * @param $name
	 * @param $id
	 *
	 * @return string
	 */
	public function buildCheckbox($name, $id)
	{
		return '<input type="checkbox" name="checkboxStatus' . $name . '[]" value="' . $id . '"/>';
	}

	/**
	 * Create a link for modify an element
	 *
	 * @param $link
	 *
	 * @return string
	 */
	public function buildLinkForModify($link)
	{
		return '<a href="' . $link . '">Modifier</a>';
	}
}