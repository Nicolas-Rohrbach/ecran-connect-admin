<?php

/**
 * Plugin Name:       Ecran connecté ADMIN
 * Plugin URI:        https://github.com/Nicolas-Rohrbach/plugin-ecran-connecte
 * Description:       Plugin écrans connectés de l'AMU, ce plugin permet de générer des fichiers ICS. Ces fichiers sont ensuite lus pour pouvoir afficher l'emploi du temps de la personne connectée. Ce plugin permet aussi d'afficher la météo, des informations, des alertes. Tant en ayant une gestion des utilisateurs et des informations.
 * Version:           0.1
 * Author:            Nicolas Rohrbach
 * License:           GNU General Public License v2
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       ecran-connecte
 * GitHub Plugin URI: https://github.com/Nicolas-Rohrbach/plugin-ecran-connecte
 */

if (! defined('ABSPATH')) {
    die;
}

// Path
define('ECRAN_ADMIN_PLUG_PATH', '/wp-content/plugins/ecran_connecte_admin/');

require __DIR__ . '/autoload.php';
require_once __DIR__ . '/vendor/autoload.php';

include 'config.php';
include 'blocks.php';