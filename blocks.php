<?php

use Controllers\AlertController;
use Controllers\InformationController;

/**
 * Blocks for information
 *
 *
 *
 *
 *
 *
 */




function create_information_render_callback()
{
	if(is_page()) {
		$information = new InformationController();
		return $information->create();
	}
}

/**
 * Build a block
 */
function blockCreateInformation()
{
	wp_register_script(
		'create_information_script',
		plugins_url( '/blocks/information/create.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-data' )
	);

	register_block_type('tvconnecteeamu/add-information', array(
		'editor_script' => 'create_information_script',
		'render_callback' => 'create_information_render_callback'
	));
}
add_action('init', 'blockCreateInformation');


function display_information_render_callback()
{
	if(is_page()) {
		$information = new InformationController();
		return $information->displayAll();
	}
}

/**
 * Build a block
 */
function blockDisplayInformation()
{
	wp_register_script(
		'display_information_script',
		plugins_url( '/blocks/information/displayAll.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-data' )
	);

	register_block_type('tvconnecteeamu/display-information', array(
		'editor_script' => 'display_information_script',
		'render_callback' => 'display_information_render_callback'
	));
}
add_action('init', 'blockDisplayInformation');

function modify_information_render_callback()
{
	if(is_page()) {
		$information = new InformationController();
		return $information->modify();
	}
}

/**
 * Build a block
 */
function blockModifyInformation()
{
	wp_register_script(
		'modify_information_script',
		plugins_url( '/blocks/information/modify.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-data' )
	);

	register_block_type('tvconnecteeamu/modify-information', array(
		'editor_script' => 'modify_information_script',
		'render_callback' => 'modify_information_render_callback'
	));
}
add_action('init', 'blockModifyInformation');

/**
 * Blocks for alert
 *
 *
 *
 *
 *
 *
 *
 *
 *
 */

function create_alert_render_callback()
{
	if(is_page()) {
		$alert = new AlertController();
		return $alert->create();
	}
}

/**
 * Build a block
 */
function blockCreateAlert()
{
	wp_register_script(
		'create_alert_script',
		plugins_url( '/blocks/alert/create.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-data' )
	);

	register_block_type('tvconnecteeamu/add-alert', array(
		'editor_script' => 'create_alert_script',
		'render_callback' => 'create_alert_render_callback'
	));
}
add_action('init', 'blockCreateAlert');


function display_alert_render_callback()
{
	if(is_page()) {
		$alert = new AlertController();
		return $alert->displayAll();
	}
}

/**
 * Build a block
 */
function blockDisplayAlert()
{
	wp_register_script(
		'display_alert_script',
		plugins_url( '/blocks/alert/displayAll.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-data' )
	);

	register_block_type('tvconnecteeamu/display-alert', array(
		'editor_script' => 'display_alert_script',
		'render_callback' => 'display_alert_render_callback'
	));
}
add_action('init', 'blockDisplayAlert');

function modify_alert_render_callback()
{
	if(is_page()) {
		$alert = new AlertController();
		return $alert->modify();
	}
}

/**
 * Build a block
 */
function blockModifyAlert()
{
	wp_register_script(
		'modify_alert_script',
		plugins_url( '/blocks/alert/modify.js', __FILE__ ),
		array( 'wp-blocks', 'wp-element', 'wp-data' )
	);

	register_block_type('tvconnecteeamu/modify-alert', array(
		'editor_script' => 'modify_alert_script',
		'render_callback' => 'modify_alert_render_callback'
	));
}
add_action('init', 'blockModifyAlert');