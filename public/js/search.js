/**
 * Filter the table to find the key
 */
function search()
{
    let input, filter, table, tr, td, allTd, j, i, txtValue, hide;
    input = document.getElementById("key");
    filter = input.value.toUpperCase();
    table = document.getElementById("table");
    tr = table.childNodes[3].getElementsByTagName("tr");

    for (i = 0; i < tr.length; ++i) {
        allTd = tr[i].getElementsByTagName("td");
        for(j = 0; j < allTd.length; ++j) {
            td = allTd[j];
            if (td) {
                txtValue = td.textContent || td.innerText;
                if (txtValue.toUpperCase().indexOf(filter) > -1) {
                    ++hide;
                }
            }
        }
        if(hide > 0) {
            tr[i].style.display = "";
        } else {
            tr[i].style.display = "none";
        }
        hide = 0;
    }
}
